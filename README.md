# Challenge MercadoLibre

![GitHub Logo](/images/logomercadolibre.png)

El challenge consta de implementar una cola de mensajes utilizando un Redis y además implementar una API que nos permita abstraernos.
Esta API deberá estar conformada por 3 métodos (endpoints) que deben cumplir con el siguiente contrato:
* Pop:
     - Endpoint: /api/queue/pop
     - Método: POST
     - Respuesta:
     - Status code: 200
     - Body: {
        ‘status’: ‘ok’,
        ‘message’: <msg>
        }

* Push:
    - Endpoint: /api/queue/push
    - Método: POST
    - Body: < msg >
    - Respuesta:
    - Status code: 200
    - Body:
        {
        ‘status’: ‘ok’
        }
* Cantidad de mensajes:
    - Endpoint: /api/queue/count
    - Método: GET
    - Respuesta:
    - Status code: 200
    - Body:
        {
        ‘status’: ‘ok’,
        ‘count’: < count >
        }
        
        
## Resolución

Se utilizo Flask para realizar el challenge. *Flask* es un framework de Python. ¿Por que la decisión? Estoy desarrollando mi tesis en Python y estoy más familiarizada con este lenguaje.
Utilice tres métodos como requeria el challenge:

*   ```@app.route('/api/queue/count', methods = ['GET']) ```
*   ```@app.route('/api/queue/push', methods = ['POST']) ```
*   ```@app.route('/api/queue/pop', methods = ['POST'])```

Implemente una cola **LIFO (First in - First Out).**

A continuación se explicara con ayuda de *Postman* como se realizo el challenge:

Primero crearemos una *queue*, para ello haremos con postman varias requests a la siguiente url para realizar una queue:

http://0.0.0.0:5000/api/queue/push con **POST**

![GitHub Logo](/images/postman-push.png)

A través del método count, podemos ver el largo de nuestra *queue*:

http://0.0.0.0:5000/api/queue/count con  **GET**

![GitHub Logo](/images/postman-count.png)

Se puede visualizar en el json, que el largo de nuestra *queue* es de 18.

Por último, se visualizá el método *POP*, el cual, al ser una queue LIFO, se libera el primer mensaje.

http://0.0.0.0:5000/api/queue/pop con **POST**

![GitHub Logo](/images/postman-pop.png)

Para verificar que se realizo correctamente, se volverá a realizar una request de count. Si todo esta bien, debería dar la request count = 17.

http://0.0.0.0:5000/api/queue/count con  **GET**

![GitHub Logo](/images/postman-count2.png)

## Reproducir challenge - Docker


Se implemento para el desarrollo de la API Docker, para evitar problemas de compatibilidad y configuración:

```
cd Challenge_Mercadolibre
docker build -t sender .
docker-compose up --build
```
## Correcciones

##### Método Push:
    
 Se puede realizar un push mediante el formato json, con el nombre de la queue, y el mensaje. El formato de json aceptado es:
 {"name":"mensaje"}. En postman se puede ver un ejemplo:
    
 ![GitHub Logo](/images/postman_push_2.png)

En nuestro ejemplo, el nombre de la queue es "ransomware", y el mensaje que tiene es "wannacry":

```
{
"name":"ransomware",
"message": "wannacry"
}
```
En cuanto al status code se devuelve en la respuesta http, esto se aplica a todos los métodos (pop,push , y count)

![GitHub Logo](/images/status_push_200.png)
 
Se le hicieron modificaciones para que la API, solo tome el formato json. En caso, de que enviemos otro formato, lo valida y muestra el error. Por ejemplo:
 
 ![GitHub Logo](/images/postman_push_error.png)

Y en nuestro log, veremos error 400 (Bad Request)

![GitHub Logo](/images/status_push_400.png)
 
##### Método Count:

Al igual que en el método push, se le agrego la funcionalidad de ingresar el nombre de la queue que se quiere consultar. Además, verifica que lo ingresado sea type json, y en caso de que la queue este vacia devuelve un json con el detalle:

![GitHub Logo](/images/postman_count_2.png)
 
Log de la request:
 
![GitHub Logo](/images/status_count_200.png)
 
En el caso de la queue este vacia, el status del json será:
 
![GitHub Logo](/images/postman_count_empty.png)
  
Y en el log:
 
![GitHub Logo](/images/status_count_400.png)
 
##### Método POP:

Para liberar un mensaje, se debe realizar la request con el nombre de la queue que se quiere liberar:
 
 ![GitHub Logo](/images/postman_pop_2.png)
  
En el caso de que la queue no tenga mas mensajes para liberar, obtendremos el siguiente status:
 
![GitHub Logo](/images/postman_pop_empty.png)
   
En el log se refleja, con el código 400:
  
![GitHub Logo](/images/status_code_pop400.png)
  
 
 