from flask import Flask, jsonify, request, make_response
import redis



app = Flask(__name__)

redisClient = redis.StrictRedis(host='redis',
                                port=6379,
                                db=0)


# METODO COUNT
@app.route('/api/queue/count', methods=['GET'])
def count_msj():

    try:
        content = request.get_json()
        name = content['name']
        count = str(redisClient.llen(name))
        if count is None or "0":
            return make_response(jsonify({'status': 'OK', 'count': "The queue is empty"}), 400)
        else:
            return make_response(jsonify({'status': 'OK', 'count': count}), 200)

    except:
        return make_response(jsonify({"status": "Invalid type, Must be a json"}), 400)

# METODO PUSH
@app.route('/api/queue/push', methods=['POST'])
def push_msj():
    try:
        content = request.get_json()
        name = content['name']
        message = content['message']
        redisClient.rpush(name, message)  # pusheamos en la cola

        return make_response(jsonify({'status': 'OK'}), 200)
    except:
        return make_response(jsonify({"status": "Invalid type, Must be a json"}), 400)


# METODO POP
@app.route('/api/queue/pop', methods=['POST'])
def pop_msj():
    try:
        content = request.get_json()
        name = content['name']
        msj = str(redisClient.lindex(name, 0))
        pop = redisClient.lpop(name)
        if pop is None:
            return make_response(jsonify({'status': 'The queue is empty'}), 400)
        else:
            return make_response(jsonify({'status': 'ok', 'message': msj}), 200)

    except :
        return make_response(jsonify({"status": "Invalid type, Must be a json"}), 400)




if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
